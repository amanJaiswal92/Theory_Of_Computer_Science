#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define BUF_SIZE 345615

int main(int argc,char **argv)
{
  FILE *filePointer1,*filePointer2;
  char buffer[BUF_SIZE];
  int lengthString,i=0,j,k;
  filePointer1 = fopen(argv[1],"r");
  fread(buffer,1,BUF_SIZE,filePointer1);
  lengthString = strlen(buffer);
  filePointer2 = fopen("output.txt","w");
  if( filePointer1 == NULL )
    {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
    }
   
  while(buffer[i]!='\0')
    {
      j=i;
      j++;
      if(((buffer[i]=='/') && (buffer[j]=='*')) ||((buffer[i]=='/') && (buffer[j]=='/')))
	{
	  i=j;
	  i++;
	  j=i;
	  j++;
	  k=i;
	  while((buffer[i]=='*') && (buffer[j]=='/') || (buffer[i]!='\n'))
	    {
	      fprintf(filePointer2,"%c",buffer[k]);
	      i++;
	      j++;
	      k++;
	    }
	  fprintf(filePointer2,"\n");
	}
      else
	i++;
    }
  
  fclose(filePointer1);
  fclose(filePointer2);
  
  return 0;
}
