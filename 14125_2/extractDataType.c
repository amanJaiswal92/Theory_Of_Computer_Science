#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc,char **argv)
{
  int size,fd,i=0,j=0;
  char *buffer,*data,*temp;
  FILE *filePointer;
  if(argc!=3)
    {
      fprintf(stderr,"Uses : ./a.out <file name1> <Data Type>\n");
      return -1;
    }
  
  filePointer = fopen(argv[1],"r");
  if(filePointer == NULL)
    {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
    }
  fseek(filePointer,0,2);
  size=ftell(filePointer);
  buffer = (char *)malloc(sizeof(char)*size);
  
  fseek(filePointer,0,SEEK_SET);
  fread(buffer,1,size,filePointer);
  
  size = strlen(argv[2]);
  data = (char *)malloc(sizeof(char)*(size+1));
  strcpy(data,argv[2]);
  data[size]=' ';
  temp = (char *)malloc(sizeof(char)*(size+1));
 
  while(buffer[i]!='\0')
    {
      if(data[0]==buffer[i])
	{
	  for(j=0;j<=size;j++)
	    {
	      temp[j]=buffer[i];
	      i++;
	    }
	  
	  if (strcmp(data,temp)==0)
	    {
	      while((buffer[i]==';')||(buffer[i]!='\n'))
		{
		  fprintf(stdout,"%c",buffer[i]);
		  i++;
		}
	      fprintf(stdout,"\n");
	    }
	}
      i++;
    }
  fclose(filePointer);
  return 0;
 
}
